
const FIRST_NAME = "ANA";
const LAST_NAME = "BICA";
const GRUPA = "1090";

/**
 * Make the implementation here
 */

function initCaching() {
    var cache={};
    cache.about=0;
    cache.home=0;
    cache.contact=0;
    cache.pageAccessCounter=function(param="home"){  
        if(param.toUpperCase() == "ABOUT"){
                this.about++;
            }
        if(param.toUpperCase() == "HOME" || param === null){
                this.home++;
            }
        if(param.toUpperCase() == "CONTACT")
        {
            this.contact++;
        }
            
    };
    cache.getCache= () =>
    {
        return cache;
    }
    return cache;
}
 

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

